cmake_minimum_required(VERSION 3.12)
project(Exercise02)

set(CMAKE_CXX_STANDARD 11)

add_subdirectory(Part1)
add_subdirectory(Part2)
