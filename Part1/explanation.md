## Part 1

### Task 1
Study the 3 versions. What is the difference between them, and how would expect this to manifest in terms of
performance?

---

The first version initializes the matrix as usual with a 2D vector as we already know it from the last exercise.
On the contrary, the second version uses a 1D vector, calculating the index dependent on `i`, `j`, and the matrix
size `matSize`, a solution we already used for our sequential optimization in the last exercise. In the third version,
also a 1D vector of size `n*n` is used for holding the matrix values, but additionally a vector of `double*` is used.
This vector of pointers basically holds references to the rows of the matrix. Each pointer points to one row.

We would like to expect that the nested version is the slowest. The second version should be much faster and the
third version should be a little bit faster then the second one. As we will see later in the benchmarks, this is
not necessarily the case.

### Task 2
Use the perf tool (or an equivalent if you are using a non-Linux system) to measure CPU counters for these
benchmark versions. Study the set of available counters and try to find ones which you expect to be relevant
for this task.

---

Perf is a tool, where you can measure several CPU counters (events). Some of these events are maybe supported
by a specific machine, some maybe not. The events for counting  can be specified with a flag.

 - perf stat command: CPU counter statistics for the specified command
 - flags:
    - -d: detailed information; can be used 3 times consecutively, each time giving more detailed information
    - -e: specifies the events, which should be counted
    - -r: used to set the amount of runs, which should be done. The runs are averaged and the standard deviation
    is given

Example command:
```
perf stat -e cpu-clock,context-switches,cpu-migrations,page-faults,cycles,instructions,cache-references,
cache-misses,bus-cycles ./mmul_nested_vec 1000
```
Output:
```
Timer - Multiplication: 694 ms
Ret: 0

Performance counter stats for './mmul_nested_vec 1000':

             714,286801      cpu-clock (msec)           #    0,999 CPUs utilized
                                2      context-switches           #    0,003 K/sec
                                0      cpu-migrations              #    0,000 K/sec
                        6.003      page-faults                     #    0,008 M/sec
        1.801.746.618      cycles                              #    2,522 GHz
        4.075.007.025      instructions                    #    2,26  insn per cycle
              71.633.752      cache-references         #  100,287 M/sec
              17.081.246      cache-misses                #   23,845 % of all cache refs
              70.683.913      bus-cycles                      #   98,957 M/sec

        0,714742556 seconds time elapsed
```

Our measurements for the three version with all used events can be seen in the next task.

### Task 3
Add the measurement of CPU counters and all 3 configuration versions to your benchmark regimen and document
all findings. You should also update the way you perform benchmarks according to the points discussed (e.g.
multiple runs, median calculation, etc.).

---

The updated benchmark regimen can be found in the [updated_benchmark_regimen.md](./updated_benchmark_regimen.md). The
results are shown below.

The ouput for our benchmarks can be found in [scripts](../scripts/output). The folder contains csv as well as html files.
We used an updated version of our last benchmark regimen written in python that runs the perf command, aggregates the results, writes them to csv files and finally plots
them into graphs. We generate one graph per measure, the normal output of the program (chrono time) as well as several events of 
the perf tool. 

The perf events we are using: 

```
cpu-clock
task-clock 
context-switches 
cpu-migrations 
page-faults 
cycles
instructions 
branches 
branch-misses 
L1-dcache-loads 
L1-dcache-load-misses 
L1-dcache-stores 
LLC-loads
LLC-stores
cache-references 
cache-misses
bus-cycles
dTLB-loads
dTLB-load-misses
```
Other events were not supported by all our machines:
```
LLC-prefetches
stalled-cycles-frontend
stalled-cycles-backend
LLC-load-misses
dTLB-prefetch-misses
```

The more interesting events were the ones regarding the caches, like cache hits or misses (eg. L1-dcache-loads, L1-dcache-load-misses).
Other than that cpu-clock and task-clock, cylces and stalled cycles.
We were generating and plotting all other events, since our benchmarking could easily generate them as well. A lot of the other events
were not generating meaningful results, thus we did not conside them further.

In general the continuous_with_multiplication Matrix version performed about 1.5 times slower than the other versions. The other versions
were always quite similar similar in terms of time or other measures like cache hits/misses. This can be seen in the normal time output graphs
as well as on their performance in other cpu events gathered from perf.
