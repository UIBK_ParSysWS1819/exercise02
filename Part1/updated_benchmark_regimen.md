## Benchmark regimen (updated version)

### Problem sizes

We changed our problem sizes for benchmarking to the following:

```
problem_sizes = [ 100, 200, 300, 400, 500, 600, 700, 800, 900, 1000, 1100, 1200, 1300, 1400, 1500 ]
```

Since our benchmarking will be automatized, we can safely add new problem sizes for a clearer picture
of the programs performance.


### Hardware platforms
We will use 3 different laptops:
- lenovo thinkpad: 16 GB RAM, i7-5600U CPU @ 2.6Ghz * 4 cores
- sony vaio: 8 GB RAM, i7-3632QM CPU @ 2.20GHz * 8 cores
- sony vaio: 8 GB RAM, i5-4200U CPU @ 1.60GHz * 4 cores

All of them use the following compiler: g++ (Ubuntu 7.3.0-27ubuntu1~18.04) 7.3.0

This will allow us to test our programs with different clock speeds and different number of cores.


### Metrics:
We use the `std::chrono::high_resolution_clock` provided in exercise 2 to get the time of the operation we want
to measure when executing the program.

Additionally we will do measuring by using the *perf-tool* with specific events.

All in all, we want to run the benchmarks multiple times and build the median of the measured data in order to get
more reliable results. By using the median, outliers won't have much impact on the results compared to using the
average. Nevertheless, we additionally also evaluate and plot results for the average, minimum and maximum values
of the measured data, because each is just one easy function call to the library we use in our benchmark script.
Thus, it is possible for us to see if there are much differences between the median and average, minimum or maximum.

These results are then used for plots and also saved to text or .csv files in tabular form.

For example, currently a Python script executes the given problem sizes on the different programs 7 times by default, builds the
median of these 7 results and saves them in the files and also uses them for the plots in the end.


### Statistical variation:
We can execute a program multiple times and calculate the median of execution times as already implied above.


### External load:
Of course, there exists the problem of the program taking more time with other programs running in parallel.
A sample solution could be to run the program externally (eg. on the cloud) a or on some VM to minimize the effect.

By using the CPU time instead of the Wall time when doing benchmarks and comparisons between different program versions,
we can measure the actual time the program is running on the CPU. This should minimize the effect of external load.
