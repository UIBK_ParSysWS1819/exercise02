## Part 2

### Task 1
Assume that the right-hand-side matrix in our matrix multiplication example is always an upper triangular matrix.
How would you use this knowledge to sequentially optimize the code? Apply your optimizations and document their
impact using your benchmarking regimen.

---

The program could be optimized in such a way, that when iterating over the columns in the second matrix, we don't
have to go until we reached the last element in the last row. We can stop as soon as we reach the elements in the
diagonal (`i == j`).

Making those optimisations should reduce the complexity of the program from O(n³) to O(n² * n/2), since on average the
innermost loop will only run for half of n. This should in theory accomplish a speedup of 2.

We tried doing this with two versions of the matrix multiplication, with a different ordering of the loops.
In the normal case of having the loops as i -> j -> k we can accomplish this by stopping the innermost loop early
until k reaches j. This resulted in double the speedup of the non-optimized version.

In the other case of having the loops as i -> k -> j we need to do this differently, by starting the innermost loop
with j = k. This version of loop ordering is much faster than the ijk version without any optimizations due to cache
improvements. By adding the optimisation we can improve the speedup by a factor of two.

### Task 2
What impact does this optimization have on the effectiveness of a simple OpenMP parallelization? How would you
optimize your parallelization strategy for this case? Implement your ideas and document their performance impact.

---

If we only optimize the outer loop, as in the "i" loop, we shouldn't have any problems when parallelizing the matrix
multiplication. Each OpenMP thread will only write in "his" own block of the matrix. Therefor we can safely add a
`pragma omp parallel for` for the first for-loop. The inner loops should consume the same amount of time, regardless of the
i iteration, as the optimization only targets the innermost loop in regards to the middle loop (`j = k; j < n;` or `k = 0; k <= j;`).
A detailed view of the optimisation results can be seen in the benchmark graphs. What can be seen is that the OpenMP versions outperform
their equivalent sequential versions almost by a factor of 2 for 4 OpenMP threads.


### Benchmarking
For benchmarking, we tried all combinations of the 3 Matrix version from part 1 with the different loop and optimization version,
as well as with or without optimizations. We also used the perf tool to try out the cache friendliness of the ikj version versus the
normal ijk version, and as expected the ikj version had much more cache hits compared to the ijk version.

We did not see any huge differences comparing the 3 matrix types of part 1 with the optimisations. It was always the similar story:
the base without any optimizations or parallelism was the slowest, next the optimized version, followed closely by the parallel non
optimized version and finally the fastest being the parralel optimized version.
Speedups where as expected: for the optimisation with a factor of 2 and for the parallel versions also a factor of 2. Combining those
we can achieve a speedup of 4.