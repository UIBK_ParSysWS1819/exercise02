import pandas as pd
import os


class CommandExecutor:
    """ Runs a command line program with parameters """
    commands = []
    prefix = ''
    program = ''
    command_str = ''

    def __init__(self, prefix, program, commands):
        self.commands = commands
        self.program = program + ' '
        self.create_command_string()
        self.prefix = prefix

    def create_command_string(self):
        self.command_str = self.prefix + ' ' + self.program
        if self.commands is None:
            return
        for comm in self.commands:
            if len(comm['parameter']) > 0:
                self.command_str += comm['command'] + ' ' + comm['parameter'] + ' '
            else:
                self.command_str += comm['command'] + ' '

    def set_commands(self, commands):
        self.commands = commands

    def set_prefix(self, prefix):
        self.prefix = prefix

    def get_command_string(self):
        return self.command_str

    def execute_command(self):
        import subprocess
        return subprocess.getoutput(self.command_str)


class PerfExecutor:

    """ Runs the perf command and puts the outputs of perf and the executable in separate folders"""
    # Detailed CPU counter statistics (includes extras) for the specified command:
    # Various CPU data TLB statistics for the specified command:
    # Various CPU last level cache statistics for the specified command:
    # to see a list of all pre-defined events: `perf list`
    events = "cpu-clock," \
             "task-clock," \
             "context-switches," \
             "cpu-migrations," \
             "page-faults," \
             "cycles," \
             "stalled-cycles-frontend," \
             "instructions," \
             "branches," \
             "branch-misses," \
             "L1-dcache-loads," \
             "L1-dcache-load-misses," \
             "L1-dcache-stores," \
             "LLC-loads," \
             "LLC-load-misses," \
             "LLC-stores," \
             "LLC-prefetches," \
             "cache-references," \
             "cache-misses," \
             "bus-cycles," \
             "dTLB-loads," \
             "dTLB-load-misses," \
             "dTLB-prefetch-misses"

    outputfolder = "./output/"
    output_perf_file_prefix = ''
    output_exec_file_prefix = ''


    customCommandList = []
    fixedCommandList = []
    perf_exe = None

    build_path = "../../exercise02/cmake-build-debug/"
    exe_file_names = ["Part1/mmul_nested_vec",
                      "Part1/mmul_cont_with_mult",
                      "Part1/mmul_cont_with_indir"
                      ]

    problem_sizes = [100, 200, 300, 400]  # , 50, 100, 200, 300, 400, 500, 600, 700, 800]
    measures_per_call = 1

    def __init__(self, build_path, exe_file_names, problem_sizes, measures_per_call, outputfolder):
        self.build_path = build_path
        self.exe_file_names = exe_file_names
        self.problem_sizes = problem_sizes
        self.measures_per_call = measures_per_call
        self.outputfolder = outputfolder
        # create folders for measurements
        if not os.path.exists(self.outputfolder):
            os.makedirs(self.outputfolder)
        if not os.path.exists(self.outputfolder + 'perf-output/'):
            os.makedirs(self.outputfolder + 'perf-output/')
        if not os.path.exists(self.outputfolder + 'executable-output/'):
            os.makedirs(self.outputfolder + 'executable-output/')
        self.output_perf_file_prefix = outputfolder + "perf-output/perf_"
        self.output_exec_file_prefix = outputfolder + "executable-output/exec_"
        ####
        self.__init_perf_commands()
        self.perf_exe = CommandExecutor('3>>perf_result.log', 'perf stat', [])

    def __set_individual_perf_commands(self):
        """ Individual settings are here"""
        # self.customCommandList.append(dict(command='-d', parameter='', description='Detailed statistics'))
        # -x ignores % and , in the last 2 rows
        self.customCommandList.append(dict(command='-x', parameter='\';\'', description='field-separator'))
        # command_list.append(dict(command='-r', parameter=str(measures_per_call), description='rerun executable x multiple times and average'))
        self.customCommandList.append(dict(command='-e', parameter=self.events, description='events'))

    def __init_perf_commands(self):
        self.__set_individual_perf_commands()
        self.__set_perf_commands_files(self.exe_file_names[0], self.problem_sizes[0])

    def __remove_benchmarks(self, executable, matrix_size):
        """Overwrite old benchmarks"""
        executable_log = str.split(executable, '/')[1]
        file_perf = self.output_perf_file_prefix + executable_log+'_' + str(matrix_size) + '.csv'
        file_txt = self.output_exec_file_prefix + executable_log + '_' + str(matrix_size) + '.txt'
        for file in [file_perf, file_txt]:
            if os.path.exists(file):  # remove file if it already exists at specified location
                os.remove(file)

    def __set_perf_commands_files(self, executable, matrix_size):
        """ Settings for execution and output are here"""
        executable_log = str.split(executable, '/')[1]
        self.fixedCommandList = []

        # https://stackoverflow.com/questions/18776262/perf-output-cannot-be-redirected-to-file
        # -o option produced a header : # started on xxxx
        self.fixedCommandList.append(dict(command='--log-fd',
                                     parameter='3',
                                     description='output of perf to file descriptor')
                                )

        # change matrix size
        self.fixedCommandList.append(dict(command=self.build_path + executable,
                                     parameter=str(matrix_size)
                                     , description='executable program')
                                )
        # change exec txt file
        self.fixedCommandList.append(dict(command='>>',
                                     parameter=self.output_exec_file_prefix +
                                               executable_log + '_' + str(matrix_size) + '.txt',
                                     description='pipe in executable output in file '))

    def clean_perf_csv(self, file, executable, problem_size):
        """ Remove non existent columns from output, convert into key value pairs """
        df = pd.read_csv(file, sep=';', names=["1", "2", "3", "4", "5", "6", "7"])
        # column 2 is empty; column 4 & 5 contain numbers that dont appear in terminal output
        # column 1,3,6,7 correspond to terminal output

        # unique names of measurements first 2 rows
        print(df["3"].unique())
        # unique names of measurements second 2 rows
        print(df["7"].unique())

        d = {}
        for index, row in df.iterrows():
            if not pd.isna(row["3"]):
                if not row["3"] in d:
                    d[row["3"]] = [row["1"]]
                else:
                    # print row['c1'], row['c2']
                    d[row["3"]].append(row["1"])
            # d[row["3"]] = [row["1"], [row["7"],row["6"]]]
            """if not pd.isna(row["7"]):
                if not row["7"] in d:
                    d[row["7"]] = [row["6"]]
                else:
                    d[row["7"]].append(row["6"])
            """
        print(d)

        # remove not distinguishable columns
        d.pop("K/sec", None)
        d.pop("M/sec", None)

        # load transposed in order to ignore non amount of values
        df_out = pd.DataFrame.from_dict(d, orient='index')
        # remove not distinguishable columns
        # df_out = df_out.drop(["K/sec", "M/sec"])
        df_out = df_out.transpose()
        df_out['executable'] = executable
        df_out['matrix_size'] = problem_size
        # Header is column name. index is line number
        df_out.to_csv(file, sep=';', encoding='utf-8', index=False, header=True)
        # return pd.DataFrame.to_dict(df_out)

    def exec_as_csv(self, filename, executable, problem_size):
        file = open(filename, "r")
        d = {"Timer - Multiplication": [], "Ret": []}
        for line in file.readlines():
            key, value = line.split(':')
            # only parse number
            d[key.strip()].append(value.strip().split()[0])
        print(d)
        df_out = pd.DataFrame.from_dict(d)
        df_out['executable'] = executable
        df_out['matrix_size'] = problem_size
        df_out.to_csv(filename.rstrip('.txt') + '.csv', sep=';', encoding='utf-8', index=False, header=True)

    def run_test(self):
        for executable in self.exe_file_names:
            for problem_size in self.problem_sizes:
                self.__remove_benchmarks(executable, problem_size)
                exec_name = str.split(executable, '/')[1]
                csv_name = self.output_perf_file_prefix + exec_name +'_' + str(problem_size) + '.csv'
                txt_name = self.output_exec_file_prefix + exec_name +'_' + str(problem_size) + '.txt'
                for i in range(self.measures_per_call):
                    self.perf_exe.set_prefix('3>>' + csv_name)
                    self.__set_perf_commands_files(executable, problem_size)
                    self.perf_exe.set_commands(self.customCommandList + self.fixedCommandList)
                    self.perf_exe.create_command_string()
                    print('Executing ' + str(self.perf_exe.get_command_string()))
                    output = self.perf_exe.execute_command()
                    print(str(output))
                    #
                self.clean_perf_csv(csv_name, exec_name, problem_size)
                self.exec_as_csv(txt_name, exec_name, problem_size)

# def main():
#     build_path = "../../exercise02/cmake-build-debug/"
#     exe_file_names = ["Part1/mmul_nested_vec",
#                       "Part1/mmul_cont_with_mult",
#                       "Part1/mmul_cont_with_indir"
#                       ]
#
#     problem_sizes = [ 100, 200]#, 300, 400, 500, 600, 700, 800, 900, 1000, 1100, 1200, 1300, 1400, 1500 ]
#     # problem_sizes = [10]
#     measures_per_call = 7
#
#     perf = PerfExecutor(build_path, exe_file_names, problem_sizes, measures_per_call)
#     perf.run_test()

# main()