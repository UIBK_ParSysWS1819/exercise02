import os
import pandas as pd

from scripts.PerfExecutor import PerfExecutor


def collect_csv_files(folderpath):
    files = []
    for i in os.listdir(folderpath):
        if i.endswith('.csv'):
            files.append(folderpath + i)
    return files


def concat_csv_files(folderpath, input_decimal):
    """read all individual csv from folder and compute measurements"""
    files = collect_csv_files(folderpath)
    # init full list with first item (floats are represented like 1,203 and later saved as 1.203)
    df_full = pd.read_csv(files[0], sep=';', decimal=input_decimal)

    # skip init item
    for file in files[1:]:
        df_temp = pd.read_csv(file, sep=';', decimal=input_decimal)
        df_full = df_full.append(df_temp, sort=True)

    # print('Full table')
    # print(df_full)
    # https://pandas.pydata.org/pandas-docs/stable/api.html#api-dataframe-stats

    # replace all non numeric values with 0
    pd_numeric = df_full.apply(pd.to_numeric, errors='coerce').fillna(0)
    pd_numeric.drop(columns=['executable'])
    pd_numeric['executable'] = df_full['executable']

    # Return the median of the values for the requested axis
    df_full_median = pd_numeric.groupby(['executable', 'matrix_size'], as_index=False).median()
    df_full_median.reset_index()
    # Return the mean of the values for the requested axis
    df_full_mean = pd_numeric.groupby(['executable', 'matrix_size'], as_index=False).mean()
    df_full_mean.reset_index()
    # This method returns the minimum of the values in the object.
    df_full_min = df_full.groupby(['executable', 'matrix_size'], as_index=False).min()
    df_full_min.reset_index()
    # This method returns the maximum of the values in the object.
    df_full_max = df_full.groupby(['executable', 'matrix_size'], as_index=False).max()
    df_full_max.reset_index()

    output_path = folderpath + "computed"
    if not os.path.exists(output_path):
        os.makedirs(output_path)

    #df_full.to_csv(folderpath + "computed/full.csv", sep=';', encoding='utf-8', index=False, header=True, decimal='.')
    df_full_mean.to_csv(folderpath + "computed/mean.csv", sep=';', encoding='utf-8', index=False, header=True,
                        decimal='.')
    df_full_median.to_csv(folderpath + "computed/median.csv", sep=';', encoding='utf-8', index=False, header=True,
                          decimal='.')
    df_full_min.to_csv(folderpath + "computed/min.csv", sep=';', encoding='utf-8', index=False, header=True,
                       decimal='.')
    df_full_max.to_csv(folderpath + "computed/max.csv", sep=';', encoding='utf-8', index=False, header=True,
                       decimal='.')


def plot_csv(filepath, output_folder):
    """csv has to have the following structure: 2 columns containing 'matrix size' and the  'executable'.
                                                the rest of the columns contain the measurements that should be plotted"""

    # source https://plot.ly/python/plot-data-from-csv/
    import plotly.offline as py
    import plotly.graph_objs as go
    import pandas as pd

    input_option = filepath.split('/')[-1:][0].split('.')[0]

    df = pd.read_csv(filepath, sep=';')
    col_names = df.columns.values.tolist()

    # print('matrix sizes')
    # print(df['matrix_size'])
    #
    # print('cpu-clock')
    # print(df['cpu-clock'])
    #
    # print("executables")
    # print(df["executable"].unique())
    #
    # print('column names')
    # print(col_names)

    for measurement in col_names:
        if measurement == 'executable' or measurement == 'matrix_size':
            continue
        else:
            data = []
            for exe in df["executable"].unique():
                # get entries of the current executable
                exe_graph = df.loc[df['executable'] == exe]
                # print(exe_graph)
                data.append(go.Scatter(x=exe_graph['matrix_size'], y=exe_graph[measurement], mode='lines',
                                       name=measurement + '_' + exe))

            layout = dict(title='Measurements for ' + measurement + ' per size' + '( ' + input_option + ' )',
                          xaxis=dict(title='matrix_size'),#, type='log', autorange=True),
                          yaxis=dict(title='Amount')#, type='log', autorange=True)
                          )

            fig = go.Figure(data=data, layout=layout)
            # fig = go.Figure(data=[trace2, trace3], layout=layout)

            output_path = output_folder + input_option + '/'
            if not os.path.exists(output_path):
                os.makedirs(output_path)

            # Plot data in the notebook
            py.plot(fig, filename=output_path + input_option + '_' + measurement + '.html', auto_open=False)


build_path = "../../exercise02/cmake-build-debug/"
exe_file_names_part1 = ["Part1/mmul_nested_vec"
                , "Part1/mmul_cont_with_mult"
                , "Part1/mmul_cont_with_indir"
                # , "Part2/mmul"
                  ]

exe_file_names_part2 = ["Part2/mmul_-DIJK_-DNESTED_VECTOR"
    , "Part2/mmul_-DIJK_-DNESTED_VECTOR_openmp"
    , "Part2/mmul_-DIJK_OPT_-DNESTED_VECTOR"
    , "Part2/mmul_-DIJK_OPT_-DNESTED_VECTOR_openmp"
    , "Part2/mmul_-DIKJ_-DNESTED_VECTOR"
    , "Part2/mmul_-DIKJ_-DNESTED_VECTOR_openmp"
    , "Part2/mmul_-DIKJ_OPT_-DNESTED_VECTOR"
    , "Part2/mmul_-DIKJ_OPT_-DNESTED_VECTOR_openmp"
    ]
output_folders = ['./Part1/output/', './Part2/output/']


problem_sizes = [100, 200, 300, 400, 500, 600, 700, 800, 900, 1000, 1100, 1200, 1300, 1400, 1500]
measures_per_call = 7
# Part 1
perf = PerfExecutor(build_path, exe_file_names_part1, problem_sizes, measures_per_call, output_folders[0])
perf.run_test()

# Part 2
perf2 = PerfExecutor(build_path, exe_file_names_part2, problem_sizes, measures_per_call, output_folders[1])
perf2.run_test()


# x = problem size
# y = col value csv
# line name = executable
for out_folder in output_folders:
    for folder in [out_folder + 'perf-output/', out_folder + 'executable-output/']:
        concat_csv_files(folder, ',')
        # plot_csv(folder + 'computed/full.csv', folder + 'graphs/')
        plot_csv(folder + 'computed/median.csv', folder + 'graphs/')
        plot_csv(folder + 'computed/mean.csv', folder + 'graphs/')
        plot_csv(folder + 'computed/max.csv', folder + 'graphs/')
        plot_csv(folder + 'computed/min.csv', folder + 'graphs/')
